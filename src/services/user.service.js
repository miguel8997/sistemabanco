import axios from 'axios';
import {authHeader,exportUser} from './auth-header';

const API_URL = 'http://localhost:8080/api/test/';
const API_URLUSER = 'http://localhost:8080/api/user/';

class UserService {

  getListarUsuarios() {
    return axios.get(API_URL + 'listar', { headers: authHeader() });
  }

  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'user', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + 'mod', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }

  getUserTodo(){
    return axios.get(API_URL + 'byRut/'+exportUser() , { headers: authHeader() });
  }

  getInfoUser(rut){
    return axios.get(API_URL + 'byRut/'+rut , { headers: authHeader() });
  }

  getInsertCreditCard(){
    return axios.get(API_URLUSER + 'insertCard/byRut/'+exportUser() , { headers: authHeader() });
  }

  getDeleteUser(id){
    return axios.get(API_URLUSER + 'removeUser/byId/'+id , { headers: authHeader() });
  }
}

export default new UserService();

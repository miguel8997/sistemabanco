import axios from 'axios';
import {authHeader,exportUser} from './auth-header';


const API_URL = 'http://localhost:8080/api/movement/';

class MovementService {


    postPagar(Movement) {
        return axios.post(API_URL + 'pagar', {
            amount: Movement.amount,
            rutEmisor: Movement.emisor,
            rutReceptor: Movement.receptor,
        });
    }

    getDeleteMovement(id){
        return axios.get(API_URL + 'removeMovement/byId/'+id , { headers: authHeader() });
      }

}

export default new MovementService();
export function authHeader() {
  let user = JSON.parse(localStorage.getItem('user2'));

  if (user && user.accessToken) {
    return { Authorization: 'Bearer ' + user.accessToken }; // for Spring Boot back-end
    // return { 'x-access-token': user.accessToken };       // for Node.js Express back-end
  } else {
    return {};
  }
}

export function exportUser() {
  let user = JSON.parse(localStorage.getItem('user2'));

  if (user && user.accessToken) {
    return user.username;
  } else {
    return {};
  }
}

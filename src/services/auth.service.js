import axios from 'axios';

const API_URL = 'http://localhost:8080/api/auth/';
import UserService from '../services/user.service';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'signin', {
        username: user.rut,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user2', JSON.stringify(response.data));
        }
      })
      .then(response=>{
        return UserService.getUserTodo();
      })
      .then(response=>{
        localStorage.setItem('user', JSON.stringify(response.data));
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('user2');
  }
  register(user) {
    return axios.post(API_URL + 'signuppp', {
      rut: user.rut,
      email: user.email,
      password: user.password,
      name:user.name,
      surnames:user.surnames,
      telephone:user.telephone
    });
  }
}

export default new AuthService();

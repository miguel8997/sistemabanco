export default class User {
  constructor(rut,email, password,name,surnames,telephone) {
    this.rut = rut;
    this.email = email;
    this.password = password;
    this.name = name;
    this.surnames = surnames;
    this.telephone = telephone;
  }
}

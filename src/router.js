import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: () => import('./views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('./views/Register.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/resumen',
      name: 'resumen',
      component: () => import('./views/Resumen.vue')
    },
    {
      path: '/tarjeta',
      name: 'tarjeta',
      component: () => import('./views/Tarjeta.vue')
    },
    {
      path: '/contactos',
      name: 'contactos',
      component: () => import('./views/Contactos.vue')
    },
    {
      path: '/movimientos',
      name: 'movimientos',
      component: () => import('./views/Movimientos.vue')
    },
    {
      path: '/usuariosActivos',
      name: 'usuariosActivos',
      component: () => import('./views/UsuariosActivos.vue')
    },
  ]
});

// router.beforeEach((to, from, next) => {
//   const publicPages = ['/login', '/register', '/home'];
//   const authRequired = !publicPages.includes(to.path);
//   const loggedIn = localStorage.getItem('user');

//   // trying to access a restricted page + not logged in
//   // redirect to login page
//   if (authRequired && !loggedIn) {
//     next('/login');
//   } else {
//     next();
//   }
// });
